#include <iostream>
#include <vector>
#include <map>
#include "room.h"
#include "character.h"
#include "item.h"
#include "area.h" // Include the Area class header

int main() {
    // Create an instance of the Area class
    Area gameArea;

    // Create Rooms
    Room startRoom("You are in a dimly lit room, there is a straight hallway.");
    Room hallway("You are in a long hallway, there is a treasure room to the left and a mysterious looking cave to the right.");
    Room treasureRoom("You have entered a treasure room!");
    Room dungeon("You enter a dungeon where a mysterious locked door is, it requires a shiny key, go onwards if you have a key.");
    Room readingUniversity("You made it to Reading University, your stuck here forever lol.");

    // Add rooms to the game area
    gameArea.AddRoom("startRoom", &startRoom);
    gameArea.AddRoom("hallway", &hallway);
    gameArea.AddRoom("treasureRoom", &treasureRoom);
    gameArea.AddRoom("dungeon", &dungeon);
    gameArea.AddRoom("readingUniversity", &readingUniversity);

    // Define exits between rooms
    gameArea.ConnectRooms("startRoom", "hallway", "onwards");
    gameArea.ConnectRooms("hallway", "treasureRoom", "left");
    gameArea.ConnectRooms("hallway", "dungeon", "right");
    gameArea.ConnectRooms("treasureRoom", "hallway", "back");
    gameArea.ConnectRooms("hallway", "startRoom", "back");
    gameArea.ConnectRooms("dungeon", "hallway", "back");
    gameArea.ConnectRooms("dungeon", "readingUniversity", "onwards");

// Create Items
    Item key("Key", "A shiny key that looks important, maybe it will open something.");
    Item sword("Sword", "A sharp sword with a golden hilt, effective against Goblins.");
// Add items to rooms
    startRoom.AddItem(key);
    bool hasKey = false;
    treasureRoom.AddItem(sword);
// Create a Player
    Player player("Alice", 100);
// Set the player's starting location
    player.SetLocation(&startRoom);
// Game loop (basic interaction)
    while (true) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (const Item& item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " <<item.GetDescription() << std::endl;
        }
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;
        int choice;
        std::cin >> choice;
        if (choice == 1) {
// Player looks around (no action required)
            std::cout << "You look around the room, nothing much to see." << std::endl;
        } else if (choice == 2) {
// Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with: ";
            std::string itemName;
            std::cin >> itemName;
            bool itemFound = false; // Declare itemFound here
            for (auto it = player.GetLocation()->GetItems().begin(); it != player.GetLocation()->GetItems().end(); ++it) {
                if (it->GetName() == itemName) {
                    player.AddItemToInventory(*it); // Add item to player's inventory
                    std::cout << "You picked up the " << itemName << ".\n";
                    player.GetLocation()->RemoveItem(*it); // Remove item from the room
                    if (itemName == "Key") {
                        hasKey = true; // Update the flag to indicate that the player has the key
                    }
                    itemFound = true;
                    break;
                }
            }
            if (!itemFound) {
                std::cout << "That item is not in this room.\n";
            }
        } else if (choice == 3) {
// Player moves to another room
            std::cout << "Enter the direction (e.g., onwards, left, right, back): ";
            std::string direction;
            std::cin >> direction;
            if (direction == "onwards" && player.GetLocation() == &dungeon) {
                // Check if the player has the key to open the door
                if (hasKey) {
                    std::cout << "You use the key to open the door and proceed onwards." << std::endl;
                    player.SetLocation(dungeon.GetExit("onwards"));
                } else {
                    std::cout << "The door is locked. You need a key to open it.\n";
                }
            } else {
                Room* nextRoom = player.GetLocation()->GetExit(direction);
                if (nextRoom != nullptr) {
                    player.SetLocation(nextRoom);
                    std::cout << "You move to the next room." << std::endl;
                } else {
                    std::cout << "You can't go that way." << std::endl;
                }
            }
        } else if (choice == 4) {
// Quit the game
            std::cout << "Goodbye!" << std::endl;
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            break;
        } else {
            std::cout << "Invalid choice. Try again." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

    }
    return 0;
}