#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "item.h"
#include "character.h"

class Room {
private:
    std::string name;
    std::string description;
    std::map<std::string, Room*> exits; // Map of exit directions to connected rooms
    std::vector<Item> items;

public:
    Room(const std::string& desc);
    Room(const std::string& roomName, const std::string& roomDescription);
    void AddExit(const std::string& direction, Room* otherRoom);  // Add an exit to another room
    void AddItem(const Item& item);  // Add an item to the room
    void RemoveItem(const Item& item);    // Remove an item from the room

    std::string GetDescription() const {    // Get the room's description
        return description;
    }

    Room* GetExit(const std::string& direction) const {    // Get the connected room for a given exit direction
        auto it = exits.find(direction);
        return (it != exits.end()) ? it->second : nullptr;
    }

    const std::vector<Item>& GetItems() const {
        return items;
    }

};
