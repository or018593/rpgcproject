#include <iostream>
#include "room.h"
#include "item.h"

Room::Room(const std::string& desc)
        : description(desc) {
}
Room::Room(const std::string& roomName, const std::string& roomDescription)
        : name(roomName), description(roomDescription) {
}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    auto it = std::find_if(items.begin(), items.end(),
                           [&item](const Item& roomItem) {
                               return roomItem.GetName() == item.GetName();
                           });

    if (it != items.end()) {
        items.erase(it);
        std::cout << "Removed " << item.GetName() << " from the room.\n";
    } else {
        std::cout << item.GetName() << " is not in this room.\n";
    }
}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}
