#include "area.h"
#include <fstream>
#include <sstream>
#include <iostream>

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);

    if (room1 && room2) {
        room1->AddExit(direction, room2);
        // Assuming bidirectional connection for simplicity
        room2->AddExit(oppositeDirection(direction), room1);
    } else {
        std::cerr << "Error: One or both of the rooms do not exist." << std::endl;
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::string roomName1, direction, roomName2;
        std::stringstream ss(line);
        ss >> roomName1 >> direction >> roomName2;
        Room* room1 = GetRoom(roomName1);
        Room* room2 = GetRoom(roomName2);
        if (room1 && room2) {
            ConnectRooms(roomName1, roomName2, direction);
        } else {
            std::cerr << "Error: One or both of the rooms do not exist." << std::endl;
        }
    }

    file.close();
}

std::string Area::oppositeDirection(const std::string& direction) {
    if (direction == "north") return "south";
    else if (direction == "south") return "north";
    else if (direction == "east") return "west";
    else if (direction == "west") return "east";
        // Add more directions if needed
    else return ""; // Invalid direction
}
