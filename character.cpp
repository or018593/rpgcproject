#include <iostream>
#include "character.h"

Character::Character(const std::string& charName, int initialHealth)
        : name(charName), health(initialHealth) {

}

Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

Room* Player::GetLocation() const {
    return location;
}



