#pragma once
#include <iostream>
#include <string>

class Item {
private:
    std::string name;
    std::string description;
public:
    Item(const std::string& name, const std::string& desc);
    std::string GetName() const; // Add this function declaration
    void Interact() const {
        std::cout << "You examine the " << name << ". It seems mysterious.\n";
    }
    std::string GetDescription() const;
};