#include "item.h"

Item::Item(const std::string& name, const std::string& desc)
        : name(name), description(desc) {}

std::string Item::GetDescription() const {
    return description;
}

std::string Item::GetName() const {
    return name;
}
