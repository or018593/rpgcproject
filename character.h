#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "item.h"
#include "room.h"

class Room;
class Character {
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health);

    void AddItemToInventory(const Item& item) {
        inventory.push_back(item);
        std::cout << name << " picks up the " << item.GetName() << ".\n";
    }

    void TakeDamage(int damage) {
        health -= damage;
        std::cout << name << " takes " << damage << " damage. Health now: " << health << "\n";
    }
    const std::vector<Item>& GetInventory() const;
};

class Player : public Character {

private:
    Room* location;

public:
    Player(const std::string& name, int health);
    Room* GetLocation() const; //player's current location
    const std::vector<Item>& GetItems() const; //player's inventory
    void SetLocation(Room* newLocation) {
        location = newLocation;
    }
};




